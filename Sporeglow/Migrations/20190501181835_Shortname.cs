﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sporeglow.Migrations
{
    public partial class Shortname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Shortname",
                table: "Game",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Shortname",
                table: "Game");
        }
    }
}
