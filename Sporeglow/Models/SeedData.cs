﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Sporeglow.Models;
using System;
using System.Linq;

namespace Sporeglow.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new SporeglowContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<SporeglowContext>>()))
            {
                // Look for any games.
                if (context.Game.Any())
                {
                    return;   // DB has been seeded
                }

                context.Game.AddRange(
                    new Game
                    {
                        Title = "Halo: Combat Evolved",
                        Shortname = "haloce",
                        Developer = "Bungie",
                        ReleaseDate = DateTime.Parse("2001-11-15"),
                        Genre = "First-person shooter",
                        Platform = "PC, Xbox",
                        Price = 4.99M,
                        BestDealLink = "https://www.microsoft.com/en-us/p/halo-combat-evolved-anniversary/c1xfhqsgwgd1?SilentAuth=1&wa=wsignin1.0&activetab=pivot:overviewtab"
                    },

                    new Game
                    {
                        Title = "Halo 2",
                        Shortname = "halo2",
                        Developer = "Bungie",
                        ReleaseDate = DateTime.Parse("2004-11-09"),
                        Genre = "First-person shooter",
                        Platform = "PC, Xbox",
                        Price = 9.99M,
                        BestDealLink = "https://www.microsoft.com/en-us/p/halo-the-master-chief-collection-digital-bundle/brnvr30j0sj2?activetab=pivot:overviewtab"
                    },

                    new Game
                    {
                        Title = "World of Warcraft",
                        Shortname = "wow",
                        Developer = "Blizzard Entertainment",
                        ReleaseDate = DateTime.Parse("2004-11-23"),
                        Genre = "Massively multiplayer online role-playing game",
                        Platform = "PC",
                        Price = 14.99M,
                        BestDealLink = "https://us.shop.battle.net/en-us/family/world-of-warcraft"
                    },

                    new Game
                    {
                        Title = "The Witcher 3: Wild Hunt",
                        Shortname = "thewitcher3",
                        Developer = "CD Projekt Red",
                        ReleaseDate = DateTime.Parse("2015-05-19"),
                        Genre = "Role-playing game",
                        Platform = "PC, Xbox One, PlayStation 4",
                        Price = 49.99M,
                        BestDealLink = "https://buy.thewitcher.com/en_us/w3"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}