﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sporeglow.Models
{
    public class Game
    {
        public int ID { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 2)]
        public string Title { get; set; }

        [StringLength(60, MinimumLength = 1)]
        [Display(Name = "Short name")]
        public string Shortname { get; set; }

        public string Developer { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Release date")]
        public DateTime ReleaseDate { get; set; }

        [StringLength(60, MinimumLength = 2)]
        public string Genre { get; set; }

        [StringLength(30, MinimumLength = 2)]
        public string Platform { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }

        [StringLength(200, MinimumLength = 7)]
        [Display(Name = "Best deal")]
        public string BestDealLink { get; set; }
    }
}
