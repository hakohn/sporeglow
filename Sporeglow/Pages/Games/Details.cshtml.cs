﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Sporeglow.Models;

namespace Sporeglow.Pages.Games
{
    public class DetailsModel : PageModel
    {
        private readonly Sporeglow.Models.SporeglowContext _context;

        public DetailsModel(Sporeglow.Models.SporeglowContext context)
        {
            _context = context;
        }

        public Game Game { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                Game = await _context.Game.FirstOrDefaultAsync();
            }
            else
            {
                Game = await _context.Game.FirstOrDefaultAsync(m => m.ID == id);
            }

            if (Game == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
