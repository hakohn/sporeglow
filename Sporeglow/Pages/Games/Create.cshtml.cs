﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Sporeglow.Models;
using System.Threading.Tasks;

namespace Sporeglow.Pages.Games
{
    public class CreateModel : PageModel
    {
        private readonly Sporeglow.Models.SporeglowContext _context;

        public CreateModel(Sporeglow.Models.SporeglowContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Game Game { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Game.Add(Game);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}