﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Sporeglow.Models;

namespace Sporeglow.Pages.Games
{
    public class DeleteModel : PageModel
    {
        private readonly Sporeglow.Models.SporeglowContext _context;

        public DeleteModel(Sporeglow.Models.SporeglowContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Game Game { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                Game = await _context.Game.FirstOrDefaultAsync();
            }
            else
            {
                Game = await _context.Game.FirstOrDefaultAsync(m => m.ID == id);
            }

            if (Game == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Game = await _context.Game.FindAsync(id);

            if (Game != null)
            {
                _context.Game.Remove(Game);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
