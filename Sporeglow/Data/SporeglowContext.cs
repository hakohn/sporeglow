﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Sporeglow.Models
{
    public class SporeglowContext : DbContext
    {
        public SporeglowContext (DbContextOptions<SporeglowContext> options)
            : base(options)
        {
        }

        public DbSet<Sporeglow.Models.Game> Game { get; set; }
    }
}
